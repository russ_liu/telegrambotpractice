module telgramBot

go 1.16

require (
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/rs/zerolog v1.26.0
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
