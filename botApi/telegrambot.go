package botapi

import (
	"errors"

	tgBotApi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/rs/zerolog/log"
)

var ErrUnrecognizedCommand = errors.New("unrecognized command.")

type TelegramBot struct {
	tgBotApi.BotAPI
	ResponseRules []IResponseRule
}

func NewTelegramBot(token string) (*TelegramBot, error) {
	botApi, err := tgBotApi.NewBotAPI(token)
	return &TelegramBot{BotAPI: *botApi}, err
}

func (t *TelegramBot) SetDebug(status bool) {
	t.BotAPI.Debug = status
}

func (t *TelegramBot) SendMsg(chatID int64, text string) {
	t.BotAPI.Send(tgBotApi.NewMessage(chatID, text))
}

func (t *TelegramBot) AppendResponseRules(rule IResponseRule) {
	t.ResponseRules = append(t.ResponseRules, rule)
}

func (t *TelegramBot) StartListenMessage() {
	updateChan, err := t.BotAPI.GetUpdatesChan(
		tgBotApi.UpdateConfig{
			Offset:  0,
			Timeout: 60,
		})

	if err != nil {
		log.Panic().Err(err)
		return
	}

	for update := range updateChan {
		for _, rule := range t.ResponseRules {
			if update.Message == nil || rule.GetChatId() != update.Message.Chat.ID {
				continue
			}

			messageChan := rule.RunRule(update)

			for result := range messageChan {
				switch result.Status {
				case Next:
					t.SendMsg(update.Message.Chat.ID, result.Text)
				case Eof:
					t.SendMsg(update.Message.Chat.ID, result.Text)
					rule.Stop()
					break
				case Abort:
					log.Fatal().Err(result.RuleErr)
					t.SendMsg(update.Message.Chat.ID, "Execute command failed.")
					rule.Stop()
					break
				}
			}
		}
	}
}
