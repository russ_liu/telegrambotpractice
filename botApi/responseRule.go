package botapi

import (
	action "telgramBot/actions"
	"telgramBot/libs"

	tgBotApi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/rs/zerolog/log"
)

type RuleStatus int

const (
	Next RuleStatus = iota + 1
	Abort
	Eof
)

type RuleMessage struct {
	Text    string
	Status  RuleStatus
	RuleErr error
}

type IResponseRule interface {
	GetChatId() int64
	RunRule(tgBotApi.Update) chan RuleMessage
	Stop()
	reset()
}
type ClockRule struct {
	chatId          int64
	ruleMessageChan chan RuleMessage
	shutdownChan    chan interface{}
}

func NewClockRule() *ClockRule {
	return &ClockRule{
		chatId:          libs.RUSS_CHAT_ID,
		ruleMessageChan: make(chan RuleMessage),
		shutdownChan:    make(chan interface{}),
	}
}

func (cR *ClockRule) GetChatId() int64 {
	return cR.chatId
}

func (cR *ClockRule) RunRule(update tgBotApi.Update) chan RuleMessage {
	cR.reset()
	go func() {
		for {
			select {
			case <-cR.shutdownChan:
				return
			default:
			}

			if update.Message == nil {
				cR.ruleMessageChan <- RuleMessage{"no message, skip this response", Eof, nil}
				return
			}
			log.Info().Msgf("[%s] %s", update.Message.From.UserName, update.Message.Text)

			switch update.Message.Text {
			case "/clockin":
				cR.ruleMessageChan <- RuleMessage{"starting clockin", Next, nil}
				err := action.NewClockinAction().Execute()

				if err != nil {
					cR.ruleMessageChan <- RuleMessage{"clockin failed", Eof, err}
				}
				cR.ruleMessageChan <- RuleMessage{"clockin finished", Eof, nil}
			case "/clockout":
				cR.ruleMessageChan <- RuleMessage{"starting clockout", Next, nil}
				err := action.NewClockoutAction().Execute()
				if err != nil {
					cR.ruleMessageChan <- RuleMessage{"clockout failed", Eof, err}
				}
				cR.ruleMessageChan <- RuleMessage{"clockout finished", Eof, nil}
			default:
				cR.ruleMessageChan <- RuleMessage{"I don't know the command", Eof, ErrUnrecognizedCommand}
			}

			cR.Stop()
		}
	}()
	return cR.ruleMessageChan
}

func (cR *ClockRule) Stop() {
	close(cR.shutdownChan)
	close(cR.ruleMessageChan)
}

func (cR *ClockRule) reset() {
	cR.ruleMessageChan = make(chan RuleMessage)
	cR.shutdownChan = make(chan interface{})
}
