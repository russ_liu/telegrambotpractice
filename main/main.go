package main

import (
	botapi "telgramBot/botApi"
	"telgramBot/libs"

	"github.com/rs/zerolog/log"
)

func main() {
	bot, err := botapi.NewTelegramBot(libs.BOT_TOKEN)
	if err != nil {
		log.Panic().Err(err)
	}

	bot.ResponseRules = append(bot.ResponseRules, botapi.NewClockRule())
	bot.SetDebug(false)
	bot.StartListenMessage()
}
