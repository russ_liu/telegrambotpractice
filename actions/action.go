package action

import (
	"os/exec"

	"github.com/rs/zerolog/log"
)

type ActionType int

const (
	Clockin ActionType = iota + 1
	Clockout
)

// Action抽象類別
type IAction interface {
	Execute() error
}
type action struct {
	Type ActionType
}

func (a *action) Execute() error {
	log.Info().Msgf("Execute action ,ActionType:%d", a.Type)
	return nil
}

// Action的繼承
type ClockinAction struct {
	action
}

func NewClockinAction() *ClockinAction {
	return &ClockinAction{action: action{Type: Clockin}}
}

func (a *ClockinAction) Execute() error {
	if err := a.action.Execute(); err != nil {
		return err
	}

	cmd := exec.Command("npm", "run", "clockin")
	cmd.Dir = "/pathToFile/"
	stdout, err := cmd.Output()
	log.Info().Msgf("puppeterClockin execute result:%s", string(stdout))
	return err
	// return nil
}

// Action的繼承
type ClockoutAction struct {
	action
}

func NewClockoutAction() *ClockoutAction {
	return &ClockoutAction{action: action{Type: Clockout}}
}
func (a *ClockoutAction) Execute() error {
	if err := a.action.Execute(); err != nil {
		return err
	}

	cmd := exec.Command("npm", "run", "clockout")
	cmd.Dir = "/pathToFile/"
	stdout, err := cmd.Output()
	log.Info().Msgf("puppeterClockin execute result:%s", string(stdout))
	return err
	// return nil
}
